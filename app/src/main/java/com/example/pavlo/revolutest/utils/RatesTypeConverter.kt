package com.example.pavlo.revolutest.utils

import android.arch.persistence.room.TypeConverter
import android.databinding.ObservableDouble
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class RatesTypeConverter {
    private var gson = Gson()

//    @TypeConverter
//    fun toRates(data: String?): Rates? {
//        return if (data == null) null else gson.fromJson(data, Rates::class.java)
//    }
//
//    @TypeConverter
//    fun toString(someObjects: Rates): String {
//        return gson.toJson(someObjects)
//    }

    @TypeConverter
    fun toRates(data: String?): Map<String, Double>? {
        return if (data == null) null else gson.fromJson(data, object : TypeToken<Map<String, Double>>() {}.type)
    }

    @TypeConverter
    fun toString(rates: Map<String, Double>): String {
        return gson.toJson(rates)
    }

    @TypeConverter
    fun toDouble(observableDouble: ObservableDouble): Double {
        return observableDouble.get()
    }

    @TypeConverter
    fun toObservableDouble(double: Double): ObservableDouble {
        return ObservableDouble(double)
    }
}