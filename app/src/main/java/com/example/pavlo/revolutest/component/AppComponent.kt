package com.example.pavlo.revolutest.component

import com.example.pavlo.revolutest.module.AppModule
import com.example.pavlo.revolutest.module.DatabaseModule
import com.example.pavlo.revolutest.module.RetrofitModule
import com.example.pavlo.revolutest.scope.ApplicationScope
import com.example.pavlo.revolutest.ui.rates.RatesPresenter
import dagger.Component

@ApplicationScope
@Component(modules = [AppModule::class, RetrofitModule::class, DatabaseModule::class])
interface AppComponent {
    fun inject(ratesPresenter: RatesPresenter)

    @Component.Builder
    interface Builder {
        fun build(): AppComponent

        fun appModule(appModule: AppModule): Builder
        fun retrofitModule(retrofitModule: RetrofitModule): Builder
        fun databaseModule(databaseModule: DatabaseModule): Builder
    }
}