package com.example.pavlo.revolutest.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.pavlo.revolutest.R
import com.example.pavlo.revolutest.databinding.ViewRecyclerRateElementBinding
import com.example.pavlo.revolutest.model.RatesUpdater
import com.example.pavlo.revolutest.ui.rates.RatesActivity
import com.example.pavlo.revolutest.utils.openKeyboard
import com.example.pavlo.revolutest.utils.toDoubleOrNumber
import com.example.pavlo.revolutest.utils.toStringWithFormat
import com.jakewharton.rxbinding2.view.RxView
import com.jakewharton.rxbinding2.widget.RxTextView
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.view_recycler_rate_element.view.*

class RatesAdapter: RecyclerView.Adapter<RatesAdapter.RateHolder>() {
    var mRatesUpdaterList: ArrayList<RatesUpdater>? = null
    private val mTextChangeSubject = BehaviorSubject.create<Pair<Double, Boolean>>()
    val mClickEvent = PublishSubject.create<Int>()
    var mTextChangeSubscription: Disposable? = null
    var mBindEvent: RatesActivity.BindEvent? = null

    fun setData(ratesUpdater: ArrayList<RatesUpdater>) {
        mRatesUpdaterList = ratesUpdater
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RateHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding: ViewRecyclerRateElementBinding = DataBindingUtil.inflate(inflater, R.layout.view_recycler_rate_element, parent, false)
        val viewHolder = RateHolder(binding)

        val r = RxView.clicks(viewHolder.itemView.clickView)
                .takeUntil(RxView.detaches(parent))
                .subscribe { mClickEvent.onNext(viewHolder.adapterPosition) }

        return viewHolder
    }

    override fun onBindViewHolder(holder: RateHolder, position: Int) {
        val item = mRatesUpdaterList?.get(position) ?: return

        holder.bind(item)
        mBindEvent?.bindEvent(position)
        mTextChangeSubject.subscribe(holder.actionValueChange)
    }

    fun setSubject(holder: RateHolder?) {
        mTextChangeSubscription?.dispose()
        val view = holder?.itemView?.currencyAmount ?: return

        mTextChangeSubscription = RxTextView
                .afterTextChangeEvents(view)
                .skipInitialValue()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe { data ->

                    if(mTextChangeSubscription?.isDisposed == false) {
                        val v = data?.editable().toString().toDoubleOrNumber()
                        mTextChangeSubject.onNext(Pair(v, true))
                    }
                }

        if(mTextChangeSubscription?.isDisposed == false) {
            val v = view.currencyAmount.text.toString().toDoubleOrNumber()
            mTextChangeSubject.onNext(Pair(v, false))
        }
    }

    fun unsubscribeSubject() {
        mTextChangeSubscription?.dispose()
    }

    override fun getItemCount(): Int = mRatesUpdaterList?.size ?: 0

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        mClickEvent.onComplete()
        mTextChangeSubject.onComplete()
    }

    ///

    inner class RateHolder(private val binding: ViewRecyclerRateElementBinding): RecyclerView.ViewHolder(binding.root) {

        val actionValueChange: Observer<Pair<Double, Boolean>> = object : Observer<Pair<Double, Boolean>> {
            override fun onComplete() {}
            override fun onSubscribe(d: Disposable) {}
            override fun onNext(pair: Pair<Double, Boolean>) {
                val pos = adapterPosition
                if(pos > 0) {
                    val rate = mRatesUpdaterList?.get(pos) ?: return
                    rate.multiplier = pair.first
                    if(pair.second) {
                        val fixedValue = rate.currencyAmountStock * pair.first
                        itemView.currencyAmount.setText(fixedValue.toStringWithFormat())
                    }
                }
            }
            override fun onError(e: Throwable) {}
        }

        fun startEditing() {
            itemView.currencyAmount.apply {
                setSelection(text.length)
                openKeyboard(itemView.context)
            }
        }

        fun bind(rate: RatesUpdater?) {
            binding.rate = rate
            binding.executePendingBindings()
        }
    }
}