package com.example.pavlo.revolutest.scope

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class RatesActivityScope