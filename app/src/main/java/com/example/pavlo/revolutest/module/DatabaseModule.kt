package com.example.pavlo.revolutest.module

import android.app.Application
import android.arch.persistence.room.Room
import com.example.pavlo.revolutest.db.database.AppDatabase
import com.example.pavlo.revolutest.scope.ApplicationScope
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule(var application: Application) {

    @ApplicationScope
    @Provides
    fun getDatabase(): AppDatabase {
        return Room.databaseBuilder(application, AppDatabase::class.java, AppDatabase.NAME).build()
    }

    @ApplicationScope
    @Provides
    fun getRatesDao(database: AppDatabase) = database.ratesDao()
}