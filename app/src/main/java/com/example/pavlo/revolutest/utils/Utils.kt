package com.example.pavlo.revolutest.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.example.pavlo.revolutest.model.CurrencyData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.IOException
import java.lang.Exception


class Utils {
    companion object {
        fun loadCurrenciesData(context: Context): Map<String, CurrencyData>? {
            var json: String? = null
            try {
                val file = context.assets.open("Common-Currency.json")
                val size = file.available()
                val buffer = ByteArray(size)
                file.read(buffer)
                file.close()
                json = String(buffer)
            }
            catch (ex: IOException) {
                ex.printStackTrace()
                return null
            }

            return Gson().fromJson(json, object : TypeToken<Map<String, CurrencyData>>(){}.type)
        }

        fun loadFlag(flagName: String): String? {
            return "file:///android_asset/flags/${flagName.toLowerCase()}.png"
        }
    }
}