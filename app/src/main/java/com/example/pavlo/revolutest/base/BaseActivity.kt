package com.example.pavlo.revolutest.base

import com.arellomobile.mvp.MvpAppCompatActivity

open class BaseActivity: MvpAppCompatActivity()