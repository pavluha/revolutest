package com.example.pavlo.revolutest.ui.rates

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.example.pavlo.revolutest.utils.Rate

@StateStrategyType(AddToEndStrategy::class)
interface RatesView: MvpView {

    fun updateRates(rates: ArrayList<Rate>)

    fun initData(rates: ArrayList<Rate>)
    fun setUpRecycler()

    fun moveItem(position: Int)

    fun showError(error: String)
    fun showError(error: Int)
}