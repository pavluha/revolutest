package com.example.pavlo.revolutest.model

import android.databinding.ObservableDouble
import android.databinding.ObservableField

data class RatesUpdater(
        var currencyName: ObservableField<String> = ObservableField(),
        var currencyAmount: ObservableDouble = ObservableDouble(0.0),
        var currencyAmountStock: Double = 1.0,
        var currencyFullName: ObservableField<String> = ObservableField(),
        var currencyFlagName: ObservableField<String> = ObservableField(),
        var multiplier: Double = 1.0
)