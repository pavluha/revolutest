package com.example.pavlo.revolutest.holder

import android.support.v7.widget.RecyclerView
import android.util.Log
import com.example.pavlo.revolutest.databinding.ViewRecyclerRateElementBinding
import com.example.pavlo.revolutest.model.RatesUpdater
import com.jakewharton.rxbinding2.view.RxView
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.view_recycler_rate_element.view.*

//class RateHolder(private val binding: ViewRecyclerRateElementBinding, val clickSubject: PublishSubject<Int>): RecyclerView.ViewHolder(binding.root) {
//
//    val actionValueChange: Observer<Double> = object : Observer<Double> {
//        override fun onComplete() {}
//        override fun onSubscribe(d: Disposable) {}
//        override fun onNext(koefficient: Double) {
////            val value = itemView.currencyAmount.text.toString().toDouble()
//            itemView.currencyAmount.setText(koefficient.toString())
//            Log.d("textChangeSubject3", "$koefficient")
//        }
//        override fun onError(e: Throwable) {}
//    }
//
//    val actionItemClick = RxView.clicks(itemView.rootView).subscribe {
//        clickSubject.onNext(layoutPosition)
//    }
//
//    fun bind(rate: RatesUpdater?) {
//        binding.rate = rate
//        binding.executePendingBindings()
//    }
//}