package com.example.pavlo.revolutest.db.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.example.pavlo.revolutest.db.dao.RatesDao
import com.example.pavlo.revolutest.model.RatesModel

@Database(entities = [RatesModel::class], version = AppDatabase.VERSION, exportSchema = false)
abstract class AppDatabase: RoomDatabase() {
    companion object {
        const val VERSION = 1
        const val NAME = "database"
    }

    abstract fun ratesDao(): RatesDao
}