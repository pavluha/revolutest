package com.example.pavlo.revolutest.module

import android.app.Application
import android.content.Context
import com.example.pavlo.revolutest.scope.ApplicationScope
import dagger.Module
import dagger.Provides

@Module
class AppModule(private var application: Application) {

    @ApplicationScope
    @Provides
    fun provideApplication() = application
}