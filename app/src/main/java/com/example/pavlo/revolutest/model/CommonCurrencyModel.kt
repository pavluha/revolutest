package com.example.pavlo.revolutest.model


//data class CommonCurrencyModel(
//    val USD: CurrencyData,
//)

data class CurrencyData(
        val symbol: String,
        val name: String,
        val symbol_native: String,
        val decimal_digits: Int,
        val rounding: Double,
        val code: String,
        val name_plural: String,
        val flag: String
)
