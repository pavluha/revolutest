package com.example.pavlo.revolutest.utils

import android.databinding.BindingAdapter
import android.databinding.ObservableDouble
import android.widget.EditText
import android.widget.ImageView
import com.squareup.picasso.Picasso

@BindingAdapter("android:text")
fun updateRates(editText: EditText, amount: ObservableDouble) {
    val editTextValue = editText.text.toString().toDoubleOrNumber()
    if(editTextValue == amount.get()) return

    editText.setText(amount.get().toStringWithFormat())
}

@BindingAdapter("android:countryFlag")
fun setImage(imageView: ImageView, countryFlag: String?) {
    if(countryFlag == null) return

    val ur = Utils.loadFlag(countryFlag)

    Picasso.get()
            .load(ur)
            .resize(200, 200)
            .centerCrop()
            .into(imageView)
}