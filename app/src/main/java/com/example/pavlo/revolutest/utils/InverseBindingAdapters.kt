package com.example.pavlo.revolutest.utils

import android.databinding.InverseBindingAdapter
import android.widget.EditText

@InverseBindingAdapter(attribute = "android:text")
fun getDouble(view: EditText): Double {
    return view.text.toString().toDoubleOrNumber()
}