package com.example.pavlo.revolutest

import android.app.Application
import com.arellomobile.mvp.MvpFacade
import com.example.pavlo.revolutest.component.AppComponent
import com.example.pavlo.revolutest.component.DaggerAppComponent
import com.example.pavlo.revolutest.module.AppModule
import com.example.pavlo.revolutest.module.DatabaseModule
import com.example.pavlo.revolutest.module.RetrofitModule
import com.squareup.leakcanary.LeakCanary

class App: Application() {
    companion object {
        lateinit var appComponent: AppComponent
        lateinit var application: App
    }

    override fun onCreate() {
        super.onCreate()

        LeakCanary.install(this)
        MvpFacade.init()
        application = this
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(application))
                .retrofitModule(RetrofitModule())
                .databaseModule(DatabaseModule(application))
                .build()
    }
}