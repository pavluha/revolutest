package com.example.pavlo.revolutest.api

import com.example.pavlo.revolutest.model.RatesModel
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("latest")
    fun loadRates(@Query("base") base: String = "EUR"): Observable<RatesModel>
}