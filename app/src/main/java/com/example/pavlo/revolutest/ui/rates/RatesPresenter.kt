package com.example.pavlo.revolutest.ui.rates

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.net.NetworkInfo
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.example.pavlo.revolutest.App
import com.example.pavlo.revolutest.Constants.Companion.DEFAUL_BASE
import com.example.pavlo.revolutest.R
import com.example.pavlo.revolutest.api.ApiService
import com.example.pavlo.revolutest.db.database.AppDatabase
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.experimental.launch
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@InjectViewState
class RatesPresenter: MvpPresenter<RatesView>(), LifecycleObserver {
    @Inject lateinit var api: ApiService
    @Inject lateinit var db: AppDatabase
    private var subscriptionLoadRates: Disposable? = null
    private var subscriptionDBUpdates: Disposable? = null
    private var subscriptionConnectivity: Disposable? = null
    var mBase = DEFAUL_BASE

    init {
        App.appComponent.inject(this)
    }

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        subscribeOnConnectivity()
    }

    private fun subscribeOnConnectivity() {
        subscriptionConnectivity = ReactiveNetwork.observeNetworkConnectivity(App.application)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { connectivity ->
                            when(connectivity.state()) {
                                NetworkInfo.State.DISCONNECTED -> {
                                    viewState.showError(R.string.error_connection)
                                    unsubscribe()
                                }

                                NetworkInfo.State.CONNECTED -> {
                                    startProcesses()
                                }

                                NetworkInfo.State.UNKNOWN -> viewState.showError(R.string.error_unknown)
                                else -> {}
                            }
                        },
                        { viewState.showError(R.string.error_unknown) }
                )
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    fun startProcesses() {
        updateDB()
        loadRates()
    }

    private fun updateDB() {
        if(subscriptionDBUpdates?.isDisposed == false) return

        subscriptionDBUpdates = db.ratesDao().getRates()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                        { ratesModel ->
//                            viewState.updateRates(ArrayList(ratesModel.rates.toList()))
                        },
                        { viewState.showError(R.string.error_database) }
                )
    }

    private fun loadRates() {
        if(subscriptionLoadRates?.isDisposed == false) return

        subscriptionLoadRates = api.loadRates(mBase)
                .debounce(500, TimeUnit.MILLISECONDS)
                .delay(1, TimeUnit.SECONDS)
                .repeat()
                .retry()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map { x ->
                    val newRates = mutableMapOf<String, Double>()
                    newRates[x.base] = 1.0
                    x.rates.keys.forEach {
                        newRates[it] = x.rates[it] ?: 0.0
                    }

                    x.copy(rates = newRates)
                }
                .subscribe(
                        { ratesModel ->
                            launch {
                                db.ratesDao().insert(ratesModel)
                            }
                            viewState.updateRates(ArrayList(ratesModel.rates.toList()))
                        },
                        { viewState.showError(R.string.error_unknown) }
                )
    }

    fun setBase(base: String?) {
        mBase = base ?: DEFAUL_BASE
        subscriptionLoadRates?.dispose()
        loadRates()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun unsubscribe() {
        subscriptionLoadRates?.dispose()
        subscriptionDBUpdates?.dispose()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun unsubscribeConnectivity() {
        subscriptionConnectivity?.dispose()
    }
}