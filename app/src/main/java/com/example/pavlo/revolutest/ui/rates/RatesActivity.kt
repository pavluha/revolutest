package com.example.pavlo.revolutest.ui.rates

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import com.arellomobile.mvp.presenter.InjectPresenter
import com.example.pavlo.revolutest.R
import com.example.pavlo.revolutest.adapter.RatesAdapter
import com.example.pavlo.revolutest.base.BaseActivity
import com.example.pavlo.revolutest.model.RatesUpdater
import com.example.pavlo.revolutest.utils.Rate
import com.example.pavlo.revolutest.utils.Utils
import io.reactivex.disposables.Disposable
import jp.wasabeef.recyclerview.animators.FadeInUpAnimator
import jp.wasabeef.recyclerview.animators.SlideInDownAnimator
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator
import kotlinx.android.synthetic.main.activity_rates.*
import org.jetbrains.anko.design.snackbar

class RatesActivity : BaseActivity(), RatesView {

    @InjectPresenter
    lateinit var mPresenter: RatesPresenter

    private val mRecyclerAdapter: RatesAdapter = RatesAdapter()
    private val mUpdater: ArrayList<RatesUpdater> = ArrayList()
    private var mClickSubscription: Disposable? = null
    private val bindEvent = object : BindEvent {
        override fun bindEvent(position: Int) {

            val lm = recycler.layoutManager as LinearLayoutManager
            val start = lm.findFirstVisibleItemPosition()

            if(start > 0)
                mRecyclerAdapter.unsubscribeSubject()
            else {
                if(mRecyclerAdapter.mTextChangeSubscription?.isDisposed == true) {
                    val holder = recycler.findViewHolderForAdapterPosition(0) as RatesAdapter.RateHolder
                    mRecyclerAdapter.setSubject(holder)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rates)
        lifecycle.addObserver(mPresenter)
        mPresenter.viewState.setUpRecycler()
    }

    override fun setUpRecycler() {
        recycler.layoutManager = LinearLayoutManager(this)
        recycler.adapter = mRecyclerAdapter
        mRecyclerAdapter.setData(mUpdater)
        mRecyclerAdapter.mBindEvent = bindEvent
        recycler.itemAnimator = FadeInUpAnimator(AccelerateDecelerateInterpolator()).apply { addDuration = 200 }
        mClickSubscription = mRecyclerAdapter.mClickEvent.subscribe { position -> mPresenter.viewState.moveItem(position) }
    }

    override fun moveItem(position: Int) {
        val item = mUpdater[position]
        mPresenter.setBase(item.currencyName.get())
        mUpdater.removeAt(position)
        mUpdater.add(0, item)
        mRecyclerAdapter.notifyItemMoved(position, 0)
        val holder = recycler.findViewHolderForAdapterPosition(0) as RatesAdapter.RateHolder
        holder.startEditing()
        mRecyclerAdapter.setSubject(holder)
    }

    override fun initData(rates: ArrayList<Rate>) {
        val ratesData = Utils.loadCurrenciesData(this@RatesActivity)
        rates.forEach { pair ->
            val it = RatesUpdater().apply {
                currencyName.set(pair.first)
                currencyAmount.set(pair.second)
                val item = ratesData?.get(pair.first)
                currencyFullName.set(item?.name ?: "")
                currencyFlagName.set(item?.flag ?: "")
            }

            mUpdater.add(it)
        }

        mRecyclerAdapter.notifyItemRangeInserted(0, mUpdater.size)
    }

    override fun updateRates(rates: ArrayList<Rate>) {

        if(mUpdater.isEmpty()) {
            mPresenter.viewState.initData(rates)
        }
        else {
            rates.forEach { pair ->
                val currentObject = mUpdater.find { rate -> rate.currencyName.get() == pair.first && rate.currencyName.get() != mPresenter.mBase }
                val n = currentObject?.multiplier?.times(pair.second) ?: 0.0
                currentObject?.currencyAmountStock = pair.second
                currentObject?.currencyAmount?.set(n)
            }
        }
    }

    override fun onStop() {
        super.onStop()
        mRecyclerAdapter.unsubscribeSubject()
    }

    override fun onDestroy() {
        super.onDestroy()
        mClickSubscription?.dispose()
    }

    // Errors

    override fun showError(error: String) {
        findViewById<View>(android.R.id.content).snackbar(error)
    }

    override fun showError(error: Int) {
        showError(getString(error))
    }

    interface BindEvent {
        fun bindEvent(position: Int)
    }
}
