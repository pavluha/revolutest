package com.example.pavlo.revolutest.db.dao

import android.arch.persistence.room.*
import com.example.pavlo.revolutest.model.RatesModel
import io.reactivex.Flowable

@Dao
interface RatesDao {
    @Query("SELECT * FROM RatesModel")
    fun getRates(): Flowable<RatesModel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(ratesModel: RatesModel)

    @Update
    fun update(ratesModel: RatesModel)

    @Delete
    fun delete(ratesModel: RatesModel)
}