package com.example.pavlo.revolutest.utils

import android.content.Context
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

//EditText

fun EditText.openKeyboard(context: Context) {
    this.requestFocus()
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
}

// String

fun String.toDoubleOrNumber(number: Double = 0.0): Double {
    return try {
        val format = NumberFormat.getInstance(Locale.FRANCE) // short way
        val num = format.parse(this)
        return num.toDouble()
    }
    catch (e: Exception) { number }
}

// Double

fun Double.toStringWithFormat(pattern: String = "0.##"): String? {
    return if(this == 0.0) null
    else DecimalFormat(pattern).format(this)
}