package com.example.pavlo.revolutest.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters
import android.databinding.ObservableDouble
import android.databinding.ObservableField
import com.example.pavlo.revolutest.utils.RatesTypeConverter

typealias Rates = ObservableField<Map<String, Double>>

@Entity
@TypeConverters(RatesTypeConverter::class)
data class RatesModel(
        @PrimaryKey
        val id: Long,
        val base: String,
        val date: String,
        val rates: Map<String, Double>
)

//data class Rates(
//        val AUD: Double,
//        val BGN: Double,
//        val BRL: Double,
//        val CAD: Double,
//        val CHF: Double,
//        val CNY: Double,
//        val CZK: Double,
//        val DKK: Double,
//        val GBP: Double,
//        val HKD: Double,
//        val HRK: Double,
//        val HUF: Double,
//        val IDR: Double,
//        val ILS: Double,
//        val INR: Double,
//        val ISK: Double,
//        val JPY: Double,
//        val KRW: Double,
//        val MXN: Double,
//        val MYR: Double,
//        val NOK: Double,
//        val NZD: Double,
//        val PHP: Double,
//        val PLN: Double,
//        val RON: Double,
//        val RUB: Double,
//        val SEK: Double,
//        val SGD: Double,
//        val THB: Double,
//        val TRY: Double,
//        val USD: Double,
//        val ZAR: Double
//)